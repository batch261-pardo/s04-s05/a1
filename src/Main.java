import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        phonebook.addContact(contact1);

        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");
        phonebook.addContact(contact2);

        ArrayList<Contact> contacts = phonebook.getContacts();

        if(contacts.isEmpty()){
            System.out.println("Contacts is Empty!");
        }else{
            for (Contact contact : contacts){
                System.out.println("----------------");
                System.out.println(contact.getName());
                System.out.println("----------------");
                System.out.println(contact.getName() + " has the following registered number: ");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address: ");
                System.out.println("my home in " + contact.getAddress());
            }
        }

    }
}